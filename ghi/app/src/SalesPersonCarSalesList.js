import React, {useEffect, useState} from 'react';

function SalesPersonCarSalesList() {
    const [filterValue, setFilterValue] = useState("");
    const [sales, setCarSales] = useState([]);

    const getData = async () => {
        const url = "http://localhost:8090/api/sales/"
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json();
            console.log(data)
            setCarSales(data.car_sales);

        }
    }
    useEffect (() => {
        getData()
    }, [])

    const handleChange = (event) => {
        setFilterValue(event.target.value);
    }

    const filteredPerson = () => {
        return sales.filter((sale) =>
            sale.sales_person.employee_name.toLowerCase().includes(filterValue.toLocaleLowerCase())
            )
    }

    return (
        <>
        <h1>Salesperson Car Sales</h1>
        <input onChange={handleChange} placeholder="Search name" />
            <table className="table table-striped">
                <thead>
                <tr>
                    <th>Employee Name</th>
                    <th>Employee Id</th>
                    <th>Customer name</th>
                    <th>VIN</th>
                    <th>MSRP</th>
                </tr>
                </thead>
                <tbody>
                    {filteredPerson().map((sale) => {
                        return (
                        <tr>
                            <td>{sale.sales_person.employee_name}</td>
                            <td>{sale.sales_person.employee_id}</td>
                            <td>{sale.customer}</td>
                            <td>{sale.automobile.vin}</td>
                            <td>{sale.msrp}</td>
                        </tr>
                    )
                })}
                </tbody>
            </table>
        </>
    )
}
export default SalesPersonCarSalesList;
