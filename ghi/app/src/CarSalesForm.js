import React, { useState, useEffect } from "react";


function CarSalesForm() {
  const [salespersons, setSalesPersons] = useState([]);
  const [customers, setCustomers] = useState([]);
  const [automobiles, setAutomobiles] = useState([]);

  const fetchEmployee = async () => {
    const url = "http://localhost:8090/api/salesperson/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setSalesPersons(data.sales_person);
    }
  };

  const fetchCustomer = async () => {
    const url = "http://localhost:8090/api/customer/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setCustomers(data.customer);
    }
  };

  const fetchAutomobile = async () => {
    const url = "http://localhost:8090/api/automobile/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setAutomobiles(data.automobile);
      console.log(data.automobile)
      }
    };

    useEffect(() => {
      fetchCustomer();
      fetchAutomobile();
      fetchEmployee();
  }, []);

  const [salePrice, setSalePrice] = useState("");
  const handleSalePriceChange = (e) => {
    setSalePrice(e.target.value);
  };
  const [salesperson, setSalesPerson] = useState("");
  const handleSalePersonChange = (e) => {
    setSalesPerson(e.target.value);
  };
  const [customer, setCustomer] = useState("");
  const handleCustomerChange = (e) => {
    setCustomer(e.target.value);
  };
  const [automobile, setAutomobile] = useState("");
  const handleAutomobileChange = (e) => {
    setAutomobile(e.target.value);
  };



  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.msrp = salePrice;
    data.sales_person = salesperson;
    data.customer = customer;
    data.automobile = automobile;

    const salesUrl = "http://localhost:8090/api/sales/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const salesResponse = await fetch(salesUrl, fetchConfig);

    if (salesResponse.ok) {
      setSalesPerson("");
      setCustomer("");
      setAutomobile("");
      setSalePrice("");
      fetchAutomobile();
    }
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Record a sale</h1>
          <form onSubmit={handleSubmit} id="create-customer-form">
            <div className="mb-3">
              <select onChange={handleCustomerChange} value={customer} placeholder="Customer Name" required name="customer" className="form-select">
                <option value="">Choose a customer</option>
                {customers.map((customer) => {
                  return (
                    <option key={ customer.id } value={ customer.id }>
                      { customer.name }
                    </option>
                  );
                })}
              </select>
            </div>
            <div className="mb-3">
              <select onChange={handleSalePersonChange} value={salesperson} placeholder="Sales Person" required name="sales_person" type="text" id="sales_person" className="form-select">
                <option value="">Choose a Sales Person</option>
                {salespersons.map((salesperson) => {
                  return (
                    <option key={salesperson.id } value={salesperson.id}>
                      {salesperson.employee_name}
                    </option>
                  );
                })}
              </select>
            </div>
            <div className="mb-3">
              <select onChange={handleAutomobileChange} value={automobile} placeholder="Automobile" required name="automobile" type="text" id="automobile" className="form-select">
                <option value="">Choose an Automobile</option>
                {automobiles.map((automobile) => {
                  return (
                    <option key={ automobile.import_href } value={ automobile.import_href }>
                      { automobile.vin }
                    </option>
                  );
                })}
              </select>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleSalePriceChange} value={salePrice} placeholder="MSRP" required name="sale_price" type="number" id="sale_price" className="form-control"/>
              <label htmlFor="sale_price">MSRP</label>
            </div>
            <button className="btn btn-primary">Add</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default CarSalesForm;
