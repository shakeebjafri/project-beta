import React, { useState } from "react";


function SalesPersonForm() {
  const [employeeName, setEmployeeName] = useState("");
  const handleEmployeeNameChange = (e) => {
    setEmployeeName(e.target.value);
  };

  const [employeeId, setEmployeeId] = useState("");
  const handleEmployeeIdChange = (e) => {
    setEmployeeId(e.target.value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.employee_name = employeeName;
    data.employee_id = employeeId;

    const url = 'http://localhost:8090/api/salesperson/';

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(url, fetchConfig);
    console.log(response)

    if (response.ok) {
      setEmployeeName("");
      setEmployeeId("");
    }
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add Sales Person</h1>
          <form onSubmit={handleSubmit} id="create-employee-form">
            <div className="form-floating mb-3">
              <input onChange={handleEmployeeNameChange} value={employeeName} placeholder="employee_name" required name="employee_name" type="text" id="employee_name" className="form-control"/>
              <label htmlFor="name">Employee Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleEmployeeIdChange} value={employeeId} placeholder="employee_id" required name="employee_id" type="number" id="employee_id" className="form-control"/>
              <label htmlFor="employee_id">Employee Id</label>
            </div>
            <button className="btn btn-primary">Add</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default SalesPersonForm;
