import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import ManufacturerList from "./ManufacturerList";
import ManufacturerForm from "./ManufacturerForm";
import AutomobileList from "./AutomobileList";
import AutomobileForm from "./AutomobileForm";
import VehicleList from "./VehicleList";
import VehicleForm from "./VehicleForm";
import SalesPersonForm from "./SalesPersonForm";
import SalesPersonList from "./SalesPersonList";
import CustomerForm from "./CustomerForm";
import CustomerList from "./CustomerList";
import CarSalesForm from "./CarSalesForm";
import CarSalesList from "./CarSalesList";
import SalesPersonCarSalesList from "./SalesPersonCarSalesList";
import TechnicianForm from "./TechnicianForm";
import TechnicianList from "./TechnicianList";
import AppointmentForm from "./ServiceAppointmentForm";


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="manufacturers" element={<ManufacturerList />} />
          <Route path="manufacturers/new" element={<ManufacturerForm />} />
          <Route path="automobiles" element={<AutomobileList />} />
          <Route path="automobiles/new" element={<AutomobileForm />} />
          <Route path="vehicles" element={<VehicleList />} />
          <Route path="vehicles/new" element={<VehicleForm />} />
          <Route path="salesperson/new" element={<SalesPersonForm />} />
          <Route path="salesperson" element={<SalesPersonList />} />
          <Route path="customer/new" element={<CustomerForm />} />
          <Route path="customer" element={<CustomerList />} />
          <Route path="sales/new" element={<CarSalesForm />} />
          <Route path="sales/sold" element={<SalesPersonCarSalesList />} />
          <Route path="sales" element={<CarSalesList />} />
          <Route path="technicians/new" element={<TechnicianForm />} />
          <Route path="technicians" element={<TechnicianList />} />
          <Route path="appointment/new" element={<AppointmentForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
