import React from "react";

class ServiceAppointmentForm extends React.Component {
    state = {
        appointment_vin: "",
        customer: "",
        date: "",
        time: "",
        reason: "",
        technicians: []
    }

    handleChangeInput = (event) => {
        this.setState({[event.target.id]: event.target.value})
    }

    handleTechnicianChange = (event) => {
        this.setState({employee_id: event.target.value})
    }

    handleSubmit = async(event) => {
        event.preventDefault()
        const data = {...this.state}
        delete data.technicians
        const appointmentUrl = 'http://localhost:8080/api/appointments/'
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(appointmentUrl, fetchConfig);
    if (response.ok) {
        const newAppointment = await response.json()
        const cleared = {
            appointment_vin: "",
            customer: "",
            date: "",
            time: "",
            reason: "",
            technicians: ""
            }
        this.setState(cleared)
        }
    }

    componentDidMount = async() => {
        const url = 'http://localhost:8080/api/appointments/'
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            this.setState({technicians: data.technicians })
        }
    }


    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                    <h1>Create an Appointment</h1>
                    <form onSubmit={this.handleSubmit} id="create-appointment-form">
                        <div className="form-floating mb-3">
                            <input value={this.state.appointment_vin} onChange={this.handleChangeInput} placeholder="appointment_vin" required type="text" name="appointment_vin" id="appointment_vin" className="form-control"/>
                            <label htmlFor="appointment_vin">VIN</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={this.state.owner} onChange={this.handleChangeInput} placeholder="owner" name="owner" type="text" id="owner" className="form-control"/>
                            <label htmlFor="owner">Customer Name</label>
                        </div>
                            <div className="form-floating mb-3">
                            <input value={this.state.date} onChange={this.handleChangeInput} placeholder="date" name="date" type="date" id="date" className="form-control"/>
                            <label htmlFor="date">Date</label>
                            </div>

                            <div className="form-floating mb-3">
                                <input value={this.state.time} onChange={this.handleChangeInput} placeholder="time" name="time" type="time" id="time" className="form-control"/>
                                <label htmlFor="time">Time</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={this.state.reason} onChange={this.handleChangeInput} placeholder="reason" name="reason" type="text" id="reason" className="form-control"/>
                                <label htmlFor="reason">Reason</label>
                            </div>
                        <div className="mb-3">
                            <select onChange={this.handleTechnicianChange} value={this.state.employee_id} required id="employee_id" name="employee_id" className="form-select">
                                <option value="">Choose a Technician</option>
                                {this.state.technicians.map(technician => {
                                return (
                                    <option key={technician.id} value={technician.id}>
                                    {technician.id}
                                    </option>
                                )
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default ServiceAppointmentForm
