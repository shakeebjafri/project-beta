import React, { useState, useEffect } from "react";


function CustomerList() {
    const [customer, setCustomer] = useState([]);
    const fetchCustomer = async () => {
        const response = await fetch('http://localhost:8090/api/customer/');
        if (response.ok) {
            const data = await response.json();
            setCustomer(data.customer);
        }
    }

    useEffect(() => {
        fetchCustomer()
    }, []);

    return (
        <div className="pt-4">
            <h1 className="pb-2">Customer</h1>
            <table className="table table-striped">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Address</th>
                    <th>Phone Number</th>
                </tr>
            </thead>
            <tbody>
                {customer.map(customer => {
                    return (
                        <tr key={customer.id}>
                            <td>{customer.id}</td>
                            <td>{customer.name}</td>
                            <td>{customer.address}</td>
                            <td>{customer.phone_number}</td>
                        </tr>
                    );
                })}
            </tbody>
            </table>
        </div>
    );
}

export default CustomerList;
