import React, { useState } from "react";


function TechnicianForm() {
    const [name, setName] = useState("");
    const handleNameChange = (e) => {
        setName(e.target.value);
    };

    const [employeeId, setEmployeeId] = useState("");
    const handleEmployeeIdChange = (e) => {
        setEmployeeId(e.target.value);
    };

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.name = name;
        data.employee_id = employeeId;

        const url = 'http://localhost:8080/api/technician/';

        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            setName("");
            setEmployeeId("");
        }
    };

    return (
        <div className="row">
        <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a Technician</h1>
                    <form onSubmit={handleSubmit} id="create-technician-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleNameChange} value={name} placeholder="name" required name="name" type="text" id="name" className="form-control"/>
                            <label htmlFor="name">Employee Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleEmployeeIdChange} value={employeeId} placeholder="employee_id" required name="employee_id" type="number" id="employee_id" className="form-control"/>
                            <label htmlFor="employee_id">Employee Id</label>
                        </div>
                        <button className="btn btn-primary">Add</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default TechnicianForm;
