import React, { useState, useEffect } from "react";


function SalesPersonList() {
    const [salesperson, setSalesPerson] = useState([]);
    const fetchSalesPerson = async () => {
        const response = await fetch("http://localhost:8090/api/salesperson/");
        if (response.ok) {
            const data = await response.json();
            setSalesPerson(data.sales_person);
        }
    }

    useEffect(() => {
        fetchSalesPerson()
    }, []);

    return (
        <div className="pt-4">
            <h1 className="pb-2">Sales Person</h1>
            <table className="table table-striped">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Employee Name</th>
                    <th>Employee Id</th>

                </tr>
            </thead>
            <tbody>
                {salesperson.map(salesperson => {
                    return (
                        <tr key={salesperson.id}>
                            <td>{salesperson.id}</td>
                            <td>{salesperson.employee_name}</td>
                            <td>{salesperson.employee_id}</td>
                        </tr>
                    );
                })}
            </tbody>
            </table>
        </div>
    );
}

export default SalesPersonList;
