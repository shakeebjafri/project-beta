import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
          <div className="dropdown">
            <button className="btn text-white dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
              Inventory
            </button>
            <ul className="dropdown-menu">
              <li>
                <NavLink className="dropdown-item" aria-current="page" to="manufacturers">Manufacturers</NavLink>
              </li>
              <li>
                <NavLink className="dropdown-item" aria-current="page" to="manufacturers/new">Add Manufacturer</NavLink>
              </li>
              <li>
                <NavLink className="dropdown-item" aria-current="page" to="vehicles">Vehicles</NavLink>
              </li>
              <li>
                <NavLink className="dropdown-item" aria-current="page" to="vehicles/new">Add Vehicle</NavLink>
              </li>
              <li>
                <NavLink className="dropdown-item" aria-current="page" to="automobiles">Automobiles</NavLink>
              </li>
              <li>
                <NavLink className="dropdown-item" aria-current="page" to="automobiles/new">Add Automobile</NavLink>
              </li>
            </ul>
          </div>
          <div className="dropdown">
            <button className="btn text-white dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
              Sales
            </button>
            <ul className="dropdown-menu">
            <li>
                <NavLink className="dropdown-item" aria-current="page" to="salesperson/new">Add Sales Person</NavLink>
              </li>
              <li>
                <NavLink className="dropdown-item" aria-current="page" to="salesperson">Sales Person</NavLink>
              </li>
              <li>
                <NavLink className="dropdown-item" aria-current="page" to="customer/new">Add Customer</NavLink>
              </li>
              <li>
                <NavLink className="dropdown-item" aria-current="page" to="customer">Customers</NavLink>
              </li>
              <li>
                <NavLink className="dropdown-item" aria-current="page" to="sales/new">Add Sales</NavLink>
              </li>
              <li>
                <NavLink className="dropdown-item" aria-current="page" to="sales">Sales</NavLink>
              </li>
              <li>
                <NavLink className="dropdown-item" aria-current="page" to="sales/sold">Sales Person Car Sales</NavLink>
              </li>
            </ul>
          </div>
          <div className="dropdown">
            <button className="btn text-white dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
              Service
            </button>
            <ul className="dropdown-menu">
            <li>
                <NavLink className="dropdown-item" aria-current="page" to="technicians/new">Add Technician</NavLink>
              </li>
              <li>
                <NavLink className="dropdown-item" aria-current="page" to="technicians">Technicians</NavLink>
              </li>
              <li>
                <NavLink className="dropdown-item" aria-current="page" to="appointments/new">Add Appointment</NavLink>
              </li>
              <li>
                <NavLink className="dropdown-item" aria-current="page" to="appointments">Appointments</NavLink>
              </li>
              <li>
                <NavLink className="dropdown-item" aria-current="page" to="appointment/history">Appointment History</NavLink>
              </li>
            </ul>
          </div>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
