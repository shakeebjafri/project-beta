import React, { useState, useEffect } from "react";


function TechnicianList() {
    const [technician, setTechnician] = useState([]);
    const fetchTechnician = async () => {
        const response = await fetch('http://localhost:8080/api/technicians/');
        if (response.ok) {
            const data = await response.json();
            setTechnician(data.technicians);
        }
    }

    useEffect(() => {
        fetchTechnician()
    }, []);

    return (
        <div className="pt-4">
            <h1 className="pb-2">Technician</h1>
            <table className="table table-striped">
            <thead>
                <tr>
                    <th>Technician Name</th>
                    <th>Technician Id</th>

                </tr>
            </thead>
            <tbody>
                {technician.map(technician => {
                    return (
                        <tr key={technician.id}>
                            <td>{technician.id}</td>
                            <td>{technician.name}</td>
                            <td>{technician.employee_id}</td>
                        </tr>
                    );
                })}
            </tbody>
            </table>
        </div>
    );
}

export default TechnicianList;
