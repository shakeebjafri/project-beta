import { useEffect, useState } from "react";


function CarSalesList() {
  const [carsales, setCarSales] = useState([]);
  const getData = async () => {
    const response = await fetch("http://localhost:8090/api/sales/");
    if (response.ok) {
      const data = await response.json();
      setCarSales(data.car_sales);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <table className="table table-hover">
      <thead>
        <tr>
          <th>Employee Name</th>
          <th>Employee Id</th>
          <th>Customer</th>
          <th>VIN</th>
          <th>MSRP</th>
        </tr>
      </thead>
      <tbody>
        {carsales.map((carsales) => {
          return (
            <tr key={carsales.id}>
              <td>{carsales.sales_person.employee_name}</td>
              <td>{carsales.sales_person.employee_id}</td>
              <td>{carsales.customer}</td>
              <td>{carsales.automobile.vin }</td>
              <td>{carsales.msrp}</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default CarSalesList;
