function MainPage() {
  return (
    <div className="px-4 py-5 my-5 text-center">
      <h1 className="display-5 fw-bold">CarCar</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
          The premiere solution for automobile dealership
          management!
        </p>
        <img src="https://cms.podium.com/wp-content/uploads/2020/08/AdobeStock_212736650.jpg" className="img-fluid" />
      </div>
    </div>
  );
}

export default MainPage;
