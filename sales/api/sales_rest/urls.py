from django.urls import path
from .views import (
    api_list_automobile,
    api_list_sales_person,
    api_sales_person,
    api_list_customer,
    api_customer,
    api_list_car_sales,
    api_car_sales
)


urlpatterns = [
    path(
        "automobile/",
        api_list_automobile,
        name="list_automobile"
    ),
    path(
        "salesperson/",
        api_list_sales_person,
        name="list_sales_person"
    ),
    path(
        "salesperson/<int:pk>/",
        api_sales_person,
        name="detail_sales_person"
    ),
    path(
        "customer/",
        api_list_customer,
        name="list_customer"
    ),
    path(
        "customer/<int:pk>/",
        api_customer,
        name="detail_customer"
    ),
    path(
        "sales/",
        api_list_car_sales,
        name="list_sales"
    ),
    path(
        "sales/<int:pk>/",
        api_car_sales,
        name="car_sales"
    )
]
