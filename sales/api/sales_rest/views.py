import json
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .encoders import AutomobileVOEncoder, SalesPersonEncoder, CustomerEncoder, CarSalesEncoder
from .models import AutomobileVO, SalesPerson, Customer, CarSales


@require_http_methods(["GET"])
def api_list_automobile(request):
    if request.method == "GET":
        sold_vehicles = []
        for carsale in CarSales.objects.all():
            sold_vehicles.append(carsale.automobile.vin)
        automobile = AutomobileVO.objects.exclude(vin__in=sold_vehicles)
        return JsonResponse(
            {"automobile": automobile},
            encoder=AutomobileVOEncoder,
        )


@require_http_methods(["GET", "POST"])
def api_list_sales_person(request):
    if request.method == "GET":
        sales_person = SalesPerson.objects.all()
        return JsonResponse(
            {"sales_person": sales_person},
            encoder=SalesPersonEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            sales_person = SalesPerson.objects.create(**content)
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except:
            response =JsonResponse(
                {"message": "Could not create sales person"}
            )
            response.status_code = 400
            return response


@require_http_methods(["GET", "PUT", "DELETE"])
def api_sales_person(request, pk):
    if request.method == "GET":
        try:
            sales_person = SalesPerson.objects.get(id=pk)
            return JsonResponse(
                {"sales_person": sales_person},
                encoder=SalesPersonEncoder,
                safe=False
                )
        except SalesPerson.DoesNotExist:
            response = JsonResponse({"message": "Sales person does not exist"})
            response.status_code = 404
            return response
    elif request.method == "PUT":
        try:
            content = json.loads(request.body)
            sales_person = SalesPerson.objects.get(id=pk)

            props = ["employee_name", "employee_id"]
            for prop in props:
                if prop in content:
                    setattr(sales_person, prop, content[prop])
            sales_person.save()
            return JsonResponse(
                {"sales_person": sales_person},
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except SalesPerson.DoesNotExist:
            response = JsonResponse({"message": "Sales person does not exist"})
            response.status_code = 404
            return response
    else:
        try:
            sales_person = SalesPerson.objects.get(id=pk)
            sales_person.delete()
            return JsonResponse(
                {"sales_person": sales_person},
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except SalesPerson.DoesNotExist:
            return JsonResponse({"message": "Sales person does not exist"})


@require_http_methods(["GET", "POST"])
def api_list_customer(request):
    if request.method == "GET":
        customer = Customer.objects.all()
        return JsonResponse(
            {"customer": customer},
            encoder=CustomerEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(
                {"customer": customer},
                encoder=CustomerEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create customer"}
            )
            response.status_code = 400
            return response


@require_http_methods(["GET", "PUT", "DELETE"])
def api_customer(request, pk):
    if request.method == "GET":
        customer = Customer.objects.get(id=pk)
        return JsonResponse(
            {"customer": customer},
            encoder=CustomerEncoder,
            safe=False
            )
    elif request.method == "PUT":
        try:
            content = json.loads(request.body)
            customer = Customer.objects.get(id=pk)

            props = ["name", "address", "phone_number"]
            for prop in props:
                if prop in content:
                    setattr(customer, prop, content[prop])
            customer.save()
            return JsonResponse(
                {"customer": customer},
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "Customer does not exist"})
            response.status_code = 404
            return response
    else:
        try:
            customer = Customer.objects.get(id=pk)
            customer.delete()
            return JsonResponse(
                {"customer": customer},
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Customer does not exist"})


@require_http_methods(["GET", "POST"])
def api_list_car_sales(request):
    if request.method == "GET":
        car_sales = CarSales.objects.all()
        return JsonResponse(
            {"car_sales": car_sales},
            encoder=CarSalesEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            sales_person_id = content["sales_person"]
            sales_person = SalesPerson.objects.get(id=sales_person_id)
            content["sales_person"] = sales_person
        except SalesPerson.DoesNotExist:
            return JsonResponse({"message": "Sales person does not exist"})
        try:
            automobile_href = content["automobile"]
            automobile = AutomobileVO.objects.get(import_href=automobile_href)
            content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse({"message": "Automobile does not exist"})
        try:
            customer = content["customer"]
            customer = Customer.objects.get(id=customer)
            content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Customer does not exist"})
        try:
            car_sales = CarSales.objects.create(**content)
            return JsonResponse(
                {"car_sales": car_sales},
                encoder=CarSalesEncoder,
                safe=False
                )
        except CarSales.DoesNotExist:
            return JsonResponse({"message": "Customer does not exist"})
            response.status_code = 400
            return response


@require_http_methods(["GET", "PUT", "DELETE"])
def api_car_sales(request, pk):
    if request.method == "GET":
        car_sales = CarSales.objects.get(id=pk)
        return JsonResponse(
            car_sales,
            encoder=CarSalesEncoder,
            safe=False
            )
    elif request.method == "PUT":
        try:
            content = json.loads(request.body)
            car_sales = CarSales.objects.get(id=pk)

            props = ["msrp"] #changed
            for prop in props:
                if prop in content:
                    setattr(car_sales, prop, content[prop])
            car_sales.save()
            return JsonResponse(
                car_sales,
                encoder=CarSalesEncoder,
                safe=False,
            )
        except CarSales.DoesNotExist:
            response = JsonResponse({"message": "Car sales does not exist"})
            response.status_code = 404
            return response
    else:
        try:
            car_sales = CarSales.objects.get(id=pk)
            car_sales.delete()
            return JsonResponse(
                car_sales,
                encoder=CarSalesEncoder,
                safe=False,
            )
        except CarSales.DoesNotExist:
            return JsonResponse({"message": "Car sales does not exist"})
