from common.json import ModelEncoder
from .models import AutomobileVO, SalesPerson, Customer, CarSales


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "id",
        "vin",
        "import_href"
    ]

class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = [
        "id",
        "employee_name",
        "employee_id"
    ]

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "id",
        "name",
        "address",
        "phone_number"
    ]

class CarSalesEncoder(ModelEncoder):
    model = CarSales
    properties = [
        "id",
        "sales_person",
        "automobile",
        "customer",
        "msrp"
    ]
    encoders = {
        "sales_person": SalesPersonEncoder(),
        "automobile": AutomobileVOEncoder(),
        "customer": CustomerEncoder(),
    }

    def get_extra_data(self, o):
        return {"customer": o.customer.name}
