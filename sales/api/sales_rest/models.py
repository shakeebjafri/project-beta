from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    import_href = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.vin


class SalesPerson(models.Model):
    employee_name = models.CharField(max_length=100)
    employee_id = models.PositiveSmallIntegerField()

    def get_api_url(self):
        return reverse("api_list_sales_person", kwargs={"pk": self.pk})


class Customer(models.Model):
    name = models.CharField(max_length=100)
    address = models.CharField(max_length=100)
    phone_number = models.IntegerField()

    def get_api_url(self):
        return reverse("api_list_customer", kwargs={"pk": self.pk})


class CarSales(models.Model):
    sales_person = models.ForeignKey(
        SalesPerson,
        related_name="sale_person",
        on_delete=models.CASCADE,
    )
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="automobile",
        on_delete=models.CASCADE,
    )
    customer = models.ForeignKey(
        Customer,
        related_name="customer",
        on_delete=models.CASCADE,
    )
    msrp =  models.IntegerField()

    def __str__(self):
        return f"{self.sales_person} sold {self.automobile} to {self.customer} for {self.msrp}"

    def get_api_url(self):
        return reverse("api_list_car_sales", kwargs={"pk": self.pk})
