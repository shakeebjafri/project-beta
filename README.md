# CarCar

Team:

* Benjamin - Service
* Shakeeb - Sales

## Design

## Service microservice
The Service microservice has two models
    -Technician - Contains the Technician name and id
    -Appointment - Contains all the info for making an appointment.
These backend elaments work with the front end to create instances of the Appointment and Technician models and writes that to the database. The models are also used to searce, list, and delete individual instances of the Appointment Model.


## Sales microservice
-AutomobileVO integrated Inventory and Sales microservices, by polling for data from the Automobile model in Inventory microservices.
    -get_automobile is the function responsible for getting automobile data from Inventory microservices.
-SalesPerson Model was used for creating new sales person using two fields employee name and employee id.
-Customer model was used for creating new customer using three fields name, address and phone number.
-CarSalesModel was used for creating new sales with 3 fields (sales_person, automobile and customer) utilizing three foreign keys (SalesPerson, AutomobileVO and Customer) and another field for the msrp for the price.
