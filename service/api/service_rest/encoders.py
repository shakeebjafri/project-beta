from common.json import ModelEncoder
from .models import Technician, AutomobileVO, Appointment


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "id",
        "name",
        "employee_id",
    ]

class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "appointment_vin",
        "customer",
        "date",
        "time",
        "technician",
        "reason",
        "vip",
        "completed",
        "technician"
    ]

    encoders = {
        "technician": TechnicianEncoder()
    }

    def get_extra_data(self,o):
        try:
            AutomobileVO.objects.get(vin=o.appointment_vin)
            return{"vip":True,}
        except:
            return {"vip":False,}
