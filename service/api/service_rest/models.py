from django.db import models
from django.urls import reverse



class Technician(models.Model):
    name = models.CharField(max_length=50, null=True, blank=True)
    employee_id = models.SmallIntegerField(unique=True, null=True, blank=True)

    def get_api_url(self):
        return reverse("api_list_technician", kwargs={"pk": self.pk})


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True, null=True, blank=True)
    import_href = models.CharField(max_length=100, unique=True, null=True, blank=True)

    def __str__(self):
        return self.vin


class Appointment(models.Model):
    appointment_vin = models.CharField(max_length=17, null=True, blank=True)
    customer = models.CharField(max_length=50, null=True, blank=True)
    date = models.DateTimeField(auto_now=False, auto_now_add=False, null=True, blank=True)
    time = models.TimeField(auto_now=False, auto_now_add=False, null=True, blank=True)
    reason = models.CharField(max_length=200, null=True, blank=True)
    vip = models.BooleanField(default=False)
    completed = models.BooleanField(default=False)

    technician = models.ForeignKey(
        "Technician",
        related_name="appointment",
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )

    def cancel(self):
        self.canceled = True
        self.save()

    def complete(self):
        self.completed = True
        self.save()
